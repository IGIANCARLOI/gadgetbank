Autores: Lieverton Horn e Giancarlo Dalle Mole

SE possível baixe pelo git: git clone https://IGIANCARLOI@bitbucket.org/IGIANCARLOI/gadgetbank.git

1. Para executar a aplicação:

	Colocar as seguintes bibliotecas na pasta do glassfish/glassfish/lib
		Todas as jar que estão dentro da pasta Hibernate
		O driver do postgres: postgresql-9.4-1201.jdbc4.jar
		As bibliotecas do primefaces: primefaces-5.2.jar e primefaces-5.2-sources.jar
		As bibliotecas de Bean Validation.
		CDI.
	Tive que excluir a pasta lib pois o arquivo compactado estava com 30MB+


	SQL para o banco de dados:

    CREATE TABLE gadget_type
    (
      id serial NOT NULL,
      type character varying NOT NULL,
      CONSTRAINT gadget_type_pkey PRIMARY KEY (id)
    )

    CREATE TABLE gadget
    (
      id serial NOT NULL,
      name character varying NOT NULL,
      gadget_type_id integer,
      CONSTRAINT gadget_pkey PRIMARY KEY (id),
      CONSTRAINT fk_gadget__gadget_type FOREIGN KEY (gadget_type_id)
          REFERENCES gadget_type (id) MATCH SIMPLE
          ON UPDATE NO ACTION ON DELETE NO ACTION
    )

    CREATE TABLE gadget_feature
    (
      id serial NOT NULL,
      feature_name character varying NOT NULL,
      feature_description character varying NOT NULL,
      gadget_id integer NOT NULL,
      CONSTRAINT gadget_feature_pkey PRIMARY KEY (id),
      CONSTRAINT fk_gadget_feature__gadget FOREIGN KEY (gadget_id)
          REFERENCES gadget (id) MATCH SIMPLE
          ON UPDATE NO ACTION ON DELETE NO ACTION
    )

	Navegando:

	Na pagina inicial apenas clique em logar ou algo do tipo, não precisa de login e senha.
	Menu lateral é para navegação.
	Não abra url de edição ou que dependendem da navegação para ser exibida corretamente, pois irá dar
	nullPointerException
		