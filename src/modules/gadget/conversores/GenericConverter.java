package modules.gadget.conversores;

import modules.gadget.entities.GadgetType;

import java.io.Serializable;
import java.util.Map;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;


@FacesConverter("generic")
public class GenericConverter implements Converter, Serializable
{
    public Object getAsObject(FacesContext ctx, UIComponent component, String value)
    {
        if (value != null)
        {
            return this.getAttributesFrom(component).get(value);
        }

        return null;
    }

    public String getAsString(FacesContext ctx, UIComponent component, Object value)
    {

        if (value != null && !"".equals(value))
        {
            GadgetType gadgetType = (GadgetType) value;

            // adiciona item como atributo do componente
            this.addAttribute(component, gadgetType);

            Integer codigo = gadgetType.getId();
            if (codigo != null)
            {
                return String.valueOf(codigo);
            }
        }

        return (String) value;
    }

    protected void addAttribute(UIComponent component, GadgetType o)
    {
        String key = o.getId().toString(); // codigo da empresa como chave neste caso
        this.getAttributesFrom(component).put(key, o);
    }

    protected Map<String, Object> getAttributesFrom(UIComponent component)
    {
        return component.getAttributes();
    }

}