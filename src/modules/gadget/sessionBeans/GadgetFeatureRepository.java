package modules.gadget.sessionBeans;

import app.base.RepositoryImpl;
import modules.gadget.entities.GadgetFeature;

import javax.ejb.Stateless;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import java.util.List;

/**
 * Created by gianc on 05/11/2015.
 */
@Stateless
public class GadgetFeatureRepository extends RepositoryImpl<GadgetFeature>
{
    public GadgetFeatureRepository()
    {
    }

    public List<GadgetFeature> buscarTodasFeatures(int gadgetId)
    {
        TypedQuery<GadgetFeature> typedQuery = entityManager.createNamedQuery("GadgetFeature.bucarTodasFeatures",
                GadgetFeature.class);
        typedQuery.setParameter("gadgetId", gadgetId);

        return typedQuery.getResultList();
    }

    @Override
    public void atualiza(GadgetFeature entidade)
    {
        Query query = entityManager.createNamedQuery("GadgetFeature.atualiza");
        query.setParameter("featureId", entidade.getId());
        query.setParameter("newFeatureName", entidade.getFeatureName());
        query.setParameter("newFeatureDescription", entidade.getFeatureDescription());

        query.executeUpdate();
    }

    public void deletaDetached(GadgetFeature gadgetFeature)
    {
        Query query = entityManager.createNamedQuery("GadgetFeature.delete");
        query.setParameter("id", gadgetFeature.getId());

        query.executeUpdate();
    }
}
