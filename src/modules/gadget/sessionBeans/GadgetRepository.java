package modules.gadget.sessionBeans;

import app.base.RepositoryImpl;
import modules.gadget.entities.Gadget;

import javax.ejb.Stateless;
import javax.persistence.Query;

/**
 * Created by gianc on 05/11/2015.
 */
@Stateless
public class GadgetRepository extends RepositoryImpl<Gadget>
{
    public GadgetRepository()
    {
    }


    @Override
    public void atualiza(Gadget entidade)
    {
        Query query = entityManager.createNamedQuery("Gadget.atualiza");
        query.setParameter("id", entidade.getId());
        query.setParameter("name", entidade.getName());
        query.setParameter("gadgetType", entidade.getGadgetType());

        query.executeUpdate();
    }
}
