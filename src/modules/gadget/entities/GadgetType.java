package modules.gadget.entities;

import modules.gadget.conversores.SampleEntity;

import javax.persistence.*;
import javax.validation.constraints.Pattern;
//import javax.validation.constraints.Pattern;
import java.io.Serializable;
import java.util.List;

/**
 * Created by gianc on 26/10/2015.
 */
@Entity
@Table(name = "gadget_type")
@NamedQueries({
        @NamedQuery(name = "GadgetType.atualiza",
                query = "update GadgetType gt set gt.type = :newType where gt.id = :id")
})
public class GadgetType implements Serializable, SampleEntity
{
    private Integer id;

    @Pattern(regexp = "[ a-zA-Z0-9]{2,20}")
    private String type;

    private List<Gadget> gadgetList;

//------------------------------------------------------------------------------------------------------------
// Contrutores

    /**
     * Construtor padrão
     */
    public GadgetType()
    {
    }

//------------------------------------------------------------------------------------------------------------
// Inicio Getters e Setters

    @Override
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false, insertable = true, updatable = true)
    public Integer getId()
    {
        return id;
    }

    public void setId(Integer id)
    {
        this.id = id;
    }

    @Basic
    @Column(name = "type", nullable = false, insertable = true, updatable = true, length = 2147483647)
    public String getType()
    {
        return type;
    }

    public void setType(String type)
    {
        this.type = type;
    }

    // Relacionamentos

    @OneToMany(mappedBy = "gadgetType")
    public List<Gadget> getGadgetList()
    {
        return gadgetList;
    }

    public void setGadgetList(List<Gadget> gadgetList)
    {
        this.gadgetList = gadgetList;
    }

// Fim Getters e Setters
//------------------------------------------------------------------------------------------------------------

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        GadgetType that = (GadgetType) o;

        if (id != null ? !id.equals(that.id) : that.id != null) return false;
        if (type != null ? !type.equals(that.type) : that.type != null) return false;

        return true;
    }

    @Override
    public int hashCode()
    {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (type != null ? type.hashCode() : 0);
        return result;
    }
}
