package modules.gadget.entities;

import javax.persistence.*;
import javax.validation.constraints.Pattern;

/**
 * Created by gianc on 26/10/2015.
 */
@Entity
@Table(name = "gadget_feature")
@NamedQueries({
        @NamedQuery(name = "GadgetFeature.bucarTodasFeatures",
                query = "select gf " +
                        "from GadgetFeature gf " +
                        "where gf.gadget.id = :gadgetId"),
        @NamedQuery(name = "GadgetFeature.atualiza",
                query = "update GadgetFeature gf " +
                        "set gf.featureName = :newFeatureName, gf.featureDescription =:newFeatureDescription " +
                        "where gf.id = :featureId"),
        @NamedQuery(name = "GadgetFeature.delete",
                    query = "delete from GadgetFeature gf " +
                            "where gf.id = :id" )
})
public class GadgetFeature
{
    private Integer id;

    @Pattern(regexp = "[a-zA-Z áàâãéèêíïóôõöúçñÁÀÂÃÉÈÊÍÏÓÔÕÖÚÇÑ()]{2,50}")
    private String featureName;

    @Pattern(regexp = ".{2,50}")
    private String featureDescription;

    private Gadget gadget;


//------------------------------------------------------------------------------------------------------------
// Construtores

    /**
     * Construtor padrão.
     */
    public GadgetFeature()
    {
    }

//------------------------------------------------------------------------------------------------------------
// Inicio Getters e Setters

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false, insertable = true, updatable = true)
    public Integer getId()
    {
        return id;
    }

    public void setId(Integer id)
    {
        this.id = id;
    }

    @Basic
    @Column(name = "feature_name", nullable = false, insertable = true, updatable = true, length = 2147483647)
    public String getFeatureName()
    {
        return featureName;
    }

    public void setFeatureName(String featureName)
    {
        this.featureName = featureName;
    }

    @Basic
    @Column(name = "feature_description", nullable = false, insertable = true, updatable = true, length = 2147483647)
    public String getFeatureDescription()
    {
        return featureDescription;
    }

    public void setFeatureDescription(String featureDescription)
    {
        this.featureDescription = featureDescription;
    }

    // Relacionamentos

    @ManyToOne
    @JoinColumn(name = "gadget_id", referencedColumnName = "id", nullable = false)
    public Gadget getGadget()
    {
        return gadget;
    }

    public void setGadget(Gadget gadget)
    {
        this.gadget = gadget;
    }

// Fim Getters e Setters
//------------------------------------------------------------------------------------------------------------

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        GadgetFeature that = (GadgetFeature) o;

        if (id != null ? !id.equals(that.id) : that.id != null) return false;
        if (featureName != null ? !featureName.equals(that.featureName) : that.featureName != null)
            return false;
        if (featureDescription != null ? !featureDescription.equals(that.featureDescription) : that.featureDescription != null)
            return false;

        return true;
    }

    @Override
    public int hashCode()
    {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (featureName != null ? featureName.hashCode() : 0);
        result = 31 * result + (featureDescription != null ? featureDescription.hashCode() : 0);
        return result;
    }


}
