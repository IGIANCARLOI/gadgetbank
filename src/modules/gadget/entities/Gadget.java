package modules.gadget.entities;

import javax.persistence.*;
import java.util.List;

/**
 * Created by gianc on 26/10/2015.
 */
@Entity
@NamedQueries({
        @NamedQuery(name = "Gadget.atualiza",
                query = "update Gadget g " +
                        "set g.name = :name, g.gadgetType = :gadgetType " +
                        "where g.id = :id")
})
public class Gadget
{
    private Integer id;
    private String name;
    private List<GadgetFeature> gadgetFeatureList;
    private GadgetType gadgetType;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false, insertable = true, updatable = true)
    public Integer getId()
    {
        return id;
    }

    public void setId(Integer id)
    {
        this.id = id;
    }

    @Basic
    @Column(name = "name", nullable = false, insertable = true, updatable = true, length = 2147483647)
    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Gadget gadget = (Gadget) o;

        if (id != null ? !id.equals(gadget.id) : gadget.id != null) return false;
        if (name != null ? !name.equals(gadget.name) : gadget.name != null) return false;

        return true;
    }

    @Override
    public int hashCode()
    {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        return result;
    }

    @OneToMany(mappedBy = "gadget")
    public List<GadgetFeature> getGadgetFeatureList()
    {
        return gadgetFeatureList;
    }

    public void setGadgetFeatureList(List<GadgetFeature> gadgetFeatureList)
    {
        this.gadgetFeatureList = gadgetFeatureList;
    }

    @ManyToOne
    @JoinColumn(name = "gadget_type_id", referencedColumnName = "id")
    public GadgetType getGadgetType()
    {
        return gadgetType;
    }

    public void setGadgetType(GadgetType gadgetType)
    {
        this.gadgetType = gadgetType;
    }
}
