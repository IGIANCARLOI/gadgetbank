package modules.gadget.cdi;

import modules.gadget.entities.Gadget;
import modules.gadget.sessionBeans.GadgetRepository;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import java.util.List;

/**
 * Created by gianc on 05/11/2015.
 */
@Named
@RequestScoped
public class GadgetMB
{
    @EJB
    GadgetRepository gadgetRepository;

    private List<Gadget> gadgetList;

    private Gadget gadget = new Gadget();


    public List<Gadget> getGadgetList()
    {
        if (gadgetList == null)
        {
            gadgetList = gadgetRepository.buscarTodos(Gadget.class);
        }
        return gadgetList;
    }

    public Gadget getGadget()
    {
        return gadget;
    }

    public void setGadget(Gadget gadget)
    {
        this.gadget = gadget;
    }

    /**
     * Adiciona uma menssagem ao faces context atual que pode ser exibida usando um growl.
     *
     * @param summary Uma menssagem que será passada ao contexto atual.
     */
    private void addMessage(String summary, FacesMessage.Severity severity, String clientId)
    {
        FacesMessage message = new FacesMessage(severity, summary, null);
        FacesContext.getCurrentInstance().addMessage(clientId, message);
        FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
    }

    public String cancel()
    {
        addMessage("Operação cancelada", FacesMessage.SEVERITY_INFO, "1");
        return "gadgetList";
    }

    public String addGadget()
    {
        gadgetRepository.insere(gadget);

        addMessage("Regitro salvo com sucesso!", FacesMessage.SEVERITY_INFO, "1");

        return "gadgetList?no-redirect";
    }

    public String editGadget(Gadget gadget)
    {
        this.gadget = gadget;

        return "editGadget?no-redirect";
    }

    public String saveEditGadget()
    {
        gadgetRepository.atualiza(gadget);
        addMessage("Regitro editado com sucesso!", FacesMessage.SEVERITY_INFO, "1");

        return "gadgetList";
    }
}
