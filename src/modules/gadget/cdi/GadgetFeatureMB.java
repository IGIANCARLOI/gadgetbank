package modules.gadget.cdi;


import modules.gadget.entities.Gadget;
import modules.gadget.entities.GadgetFeature;
import modules.gadget.sessionBeans.GadgetFeatureRepository;

import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import java.io.Serializable;
import java.util.List;

@Named
@SessionScoped
public class GadgetFeatureMB implements Serializable
{
    @EJB
    GadgetFeatureRepository gadgetFeatureRepository;

    private Gadget gadget;
    private GadgetFeature gadgetFeature = new GadgetFeature();

    private List<GadgetFeature> gadgetFeatureList;

//------------------------------------------------------------------------------------------------------------
// Construtor

//------------------------------------------------------------------------------------------------------------
// Getters e Setters

    public String getGadgetName()
    {
        if (gadget == null)
            return "Não selecionado";

        return gadget.getName();
    }

    public Gadget getGadget()
    {
        return gadget;
    }

    public void setGadget(Gadget gadget)
    {
        this.gadget = gadget;
    }

    public GadgetFeature getGadgetFeature()
    {
        return gadgetFeature;
    }

    public void setGadgetFeature(GadgetFeature gadgetFeature)
    {
        this.gadgetFeature = gadgetFeature;
    }

    public List<GadgetFeature> getGadgetFeatureList()
    {
        gadgetFeatureList = gadgetFeatureRepository.buscarTodasFeatures(gadget.getId());

        return  gadgetFeatureList;
    }

//------------------------------------------------------------------------------------------------------------
    /**
     * Adiciona uma menssagem ao faces context atual que pode ser exibida usando um growl.
     *
     * @param summary Uma menssagem que será passada ao contexto atual.
     */
    private void addMessage(String summary, FacesMessage.Severity severity, String clientId)
    {
        FacesMessage message = new FacesMessage(severity, summary, null);
        FacesContext.getCurrentInstance().addMessage(clientId, message);
        FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
    }

    public String cancel()
    {
        addMessage("Operação cancelada pelo usuário.", FacesMessage.SEVERITY_INFO, "1");
        return "listFeature";
    }

//------------------------------------------------------------------------------------------------------------
// Métodos de CRUD

    public String listaFeatures(Gadget gadget)
    {
        this.gadget = gadget;

        return  "listFeature";
    }

    //caso venha a usar bean de request, colocar parametro que recebe um gadget
    public String addFeature()
    {
        gadgetFeature = new GadgetFeature();
        gadgetFeature.setGadget(gadget);

        return  "addFeature?no-redirect";
    }

    public String saveAddFeature()
    {
        gadgetFeatureRepository.insere(gadgetFeature);

        addMessage("Cadastro efetuado com sucesso.", FacesMessage.SEVERITY_INFO, "1");

        return "listFeature?no-redirect";
    }

    public String editFeature(GadgetFeature gadgetFeature)
    {
        this.gadgetFeature = gadgetFeature;

        return "editFeature?no-redirect";
    }

    public String saveFeature()
    {
        gadgetFeatureRepository.atualiza(gadgetFeature);

        addMessage("Edição efetuada com sucesso.", FacesMessage.SEVERITY_INFO, "1");

        return "listFeature?no-redirect";
    }

    public String delete(GadgetFeature gadgetFeature)
    {
        this.gadgetFeature = gadgetFeature;

        gadgetFeatureRepository.deletaDetached(gadgetFeature);

        return null;
    }

}
