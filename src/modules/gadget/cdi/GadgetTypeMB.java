package modules.gadget.cdi;

import modules.gadget.entities.GadgetType;
import modules.gadget.sessionBeans.GadgetTypeRepository;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;

import javax.faces.context.FacesContext;
import javax.inject.Named;

import javax.faces.application.Application;
import javax.inject.Named;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import java.io.Serializable;
import java.util.List;
import java.util.ResourceBundle;

/**
 * Created by gianc on 04/11/2015.
 */

@Named
@RequestScoped
public class GadgetTypeMB implements Serializable
{
    @EJB
    GadgetTypeRepository gadgetTypeRepository;


    private GadgetType gadgetType = new GadgetType();

    private List<GadgetType> typeList;

//------------------------------------------------------------------------------------------------------------
// Construtores

    /**
     * Construtor padrão
     */
    public GadgetTypeMB()
    {
        gadgetType = new GadgetType();
    }

//------------------------------------------------------------------------------------------------------------
// Inicio Getters e Setters

    public GadgetType getGadgetType()
    {
        return gadgetType;
    }

    public void setGadgetType(GadgetType gadgetType)
    {
        this.gadgetType = gadgetType;
    }

// Fim Getters e Setters
//------------------------------------------------------------------------------------------------------------

    public List<GadgetType> getTypeList()
    {
        if (typeList == null)
        {
            typeList = gadgetTypeRepository.buscarTodos(GadgetType.class);
        }

        return typeList;
    }

// Fim Getters e Setters
//------------------------------------------------------------------------------------------------------------

    /**
     * @param key Chave usada no resource bundle
     * @return A mensagem que esta no resource bundle
     */

    private String getResourceBundleMessage(String bundleVar, String key)
    {
        FacesContext context = FacesContext.getCurrentInstance();
        Application app = context.getApplication();
        ResourceBundle bundle = app.getResourceBundle(context, bundleVar);

        return bundle.getString(key);
    }

    /**
     * @param summary Uma menssagem que será passada ao contexto atual.
     */
    private void addMessage(String summary, FacesMessage.Severity severity)
    {
        FacesMessage message = new FacesMessage(severity, summary, null);
        FacesContext.getCurrentInstance().addMessage(null, message);
        FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
    }

    public String addGadgetType()
    {
        try
        {
            gadgetTypeRepository.insere(gadgetType);
        }
        catch (Exception e)
        {
            addMessage(getResourceBundleMessage("common", "add_error") + " | " + e.getMessage(),
                    FacesMessage.SEVERITY_INFO);
            return null;
        }

        addMessage(getResourceBundleMessage("common", "add_success"), FacesMessage.SEVERITY_INFO);
        return "addGadgetType";
    }

    public String editGadgetType()
    {
        int gadgetId = Integer.parseInt(FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("gadgetTypeId"));
        gadgetType = gadgetTypeRepository.buscaPorId(GadgetType.class, gadgetId);

        return "editGadgetType?faces-redirect=false";
    }

    public String saveEditGadgetType()
    {
        gadgetTypeRepository.atualiza(gadgetType);

        addMessage(getResourceBundleMessage("common", "edit_success"), FacesMessage.SEVERITY_INFO);

        return "listGadgetType";
    }

    public String cancelEditGadgetType()
    {
        addMessage(getResourceBundleMessage("common", "operation_canceled"), FacesMessage.SEVERITY_INFO);
        return "listGadgetType";
    }

}
